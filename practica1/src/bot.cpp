#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include <error.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{	char *proyeMemoria;
	int f;
	float posRaqueta1;
	DatosMemCompartida* pMComp;
	f=open("/tmp/datos.txt",O_RDWR,0600);
	if(f==-1){
		printf("Error al abrir el fichero\n");
		exit(1);
	}
	proyeMemoria=(char*)mmap(NULL,sizeof(*(pMComp)),PROT_WRITE|PROT_READ,MAP_SHARED,f,0);
	if(proyeMemoria<0){
	printf("Error al proyectar el fichero\n");
	exit(1);
	}

	if(close(f)<0)
	{
	printf("Error al cerrar el fichero\n");
	exit(1);
	}
	pMComp=(DatosMemCompartida*)proyeMemoria;
	while(1)
	{
		usleep(25000);
		posRaqueta1=((pMComp->raqueta1.y2+pMComp->raqueta1.y1)/2);
		if(posRaqueta1<pMComp->esfera.centro.y)
			pMComp->accion=1;
		else if(posRaqueta1>pMComp->esfera.centro.y)
			pMComp->accion=-1;
		else 
			pMComp->accion=0;
	}

	if(munmap(proyeMemoria,sizeof(*(pMComp)))<0){
	printf("Error al desproyectar el fichero\n");
	exit(1);
	}
	
}
