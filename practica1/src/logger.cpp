
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include<errno.h>
int main (int argc, char argv[])
{
	int aux,error;
	char buff[200];
	error=mkfifo("/tmp/MIFIFO",0600);
	if(errno!=EEXIST && error <0){
	perror("No se puede crear el FIFO");
	return(1);
	}
	
	int fd=open("/tmp/MIFIFO",O_RDONLY);
	if(fd<0){
		perror("Error al abrir el fifo");
		return(1);
	}
	
	while(1)
	{
		aux=read(fd,buff,sizeof(buff));
		
		if(aux<0)
		{	
			perror("Error en la lectura");
			return(1);
		}
		if(aux==0)
		{	
			perror("Programa finalizado");
			return(1);
		}
		printf("%s\n",buff);
	}
	if(close(fd)==-1){
	perror("Error al cerrar el fifo");
	return(1);
	}
	unlink("/tmp/MIFIFO");
	return (0);
}

